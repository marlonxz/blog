<?php

defined('BASEPATH') OR exit('No direct script access allowed');
$lang['validate_passwd'] = "La contraseña debe de tener al menos una letra en mayúscula, minúscula, número y no menos de 8 carácteres.";
$lang['validate_same_passwd'] = "La contraseña debe ser a la almacenado en el sistema";
